-- MySQL dump 10.13  Distrib 5.5.15, for osx10.6 (i386)
--
-- Host: localhost    Database: library
-- ------------------------------------------------------
-- Server version	5.5.15

USE library;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;



--
-- Table structure for table book
--

DROP TABLE IF EXISTS book;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE book (
  #id int(11) NOT NULL AUTO_INCREMENT,
  isbn varchar(13) NOT NULL,
  title varchar(60) DEFAULT NULL,
  author varchar(45) DEFAULT NULL,
  format varchar(45) DEFAULT NULL,
  year varchar(45) DEFAULT NULL,
  category varchar(45) DEFAULT NULL,
  bestseller boolean DEFAULT FALSE,
  description blob,
  picture varchar(60) DEFAULT NULL,
  copiesAvailable int(5) default 0,
  copiesOnLoan int(5) default 0,
  PRIMARY KEY (isbn)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table book
--

#(:Book{title: "A Clash of Kings", author: "George R. R. Martin", category: "Fiction", price(€): 9.50, rating/5: 4.5}),
#(:Book{title: "A Song of Ice and Fire", author: "George R. R. Martin", category: "Fiction", price(€): 9.00, rating/5: 4.5}),
#(:Book{title: "The Hobbit", author: "J. R. R. Tolkien", category: "Fiction", price(€): 7.00, rating/5: 4}),
#(:Book{title: "The Martian", author: "Andy Weir", category: "Fiction", price(€): 12.00, rating/5: 4.5}),

#(:Book{title: "Mary Berry's Baking Bible", author: "Mary Berry", category: "Food & Drink", price(€): 20.50, rating/5: 4.5}),
#(:Book{title: "Kitchen Hero : Great Food for Less", author: "Donal Skehan", category: "Food & Drink", price(€): 16.50, rating/5: 4.5}),
#(:Book{title: "Pronto!", author: "Gino D'Acampo", category: "Food & Drink", price(€): 17.99, rating/5: 4}),


#isbn, title, author, format, year, category, bestseller, description, picture, copiesAvailable, copiesOnLoan
LOCK TABLES book WRITE;
/*!40000 ALTER TABLE book DISABLE KEYS */;
INSERT INTO book VALUES 
(9781847670830, 'The Audacity of Hope','Barack Obama', 'Paperback', '2008', 'Biographies', true, 'Barack Obama\'s success in becoming President of the United States meant that he dramatically changed the face that his country presented to the world. In this bestselling book, Barack Obama discusses the importance of empathy in politics, his hopes for a different America with different policies, and how the ideals of its democracy can be renewed.','theaudacityofhope.jpg', 6, 1),
(9781473689305, 'Blowing the Bloody Doors Off','Michael Caine', 'Audiobook', '2018', 'Biographies', false, 'Hollywood legend and British national treasure Sir Michael Caine shares the wisdom, stories, insight and skills that life has taught him in his remarkable career - and now his 85th year. With brilliant new insight into his life and work and with his wonderful gift for story, this is Caine at his wise and entertaining best.','blowingthebloodydoorsoff.jpg', 10, 4),
(9781409183747, 'Listening to the Animals: Becoming The Supervet','Noel Fitzpatrick', 'Paperback', '2018','Biographies', false, 'A powerful, heart-warming and inspiring memoir from the UK\'s most famous and beloved vet, Professor Noel Fitzpatrick - star of the Channel 4 series The Supervet.Growing up on the family farm in Ballyfin, Ireland, Noel\'s childhood was spent tending to the cattle and sheep, the hay and silage, the tractors and land, his beloved sheepdog Pirate providing solace from the bullies that plagued him at school. It was this bond with Pirate, and a fateful night spent desperately trying to save a newborn lamb, that inspired Noel to enter the world of veterinary science - and set him on the path to becoming The Supervet.','listeningtotheanimals.jpg', 4, 2),
(9781473664968, 'Adventures of a Young Naturalist','Sir David Attenborough', 'Hardback', '2018', 'Biographies', true, 'In 1954, a young television presenter named David Attenborough was offered the opportunity of a lifetime - to travel the world finding rare and elusive animals for London Zoo\'s collection, and to film the expeditions for the BBC. Now \'the greatest living advocate of the global ecosystem\' this is the story of the voyages that started it all.', 'adventuresofayoungnaturalist.jpg', 10, 2),
(9780553175219, 'A Brief History Of Time','Stephen Hawking', 'Paperback', '1995', 'Science', true, 'Was there a beginning of time? Could time run backwards? Is the universe infinite or does it have boundaries? These are just some of the questions considered in an internationally acclaimed masterpiece which begins by reviewing the great theories of the cosmos from Newton to Einstein, before delving into the secrets which still lie at the heart of space and time.','abriefhistoryoftime.jpg', 3, 6),
(9788179925911, 'The Theory of Everything','Stephen Hawking', 'Audiobook','2007','Science', false, 'Based on a series of lectures given at Cambridge University, Professor Hawking\'s work introduced "the history of ideas about the universe" as well as today\'s most important scientific theories about time, space, and the cosmos in a clear, easy-to-understand way. "The Theory of Everything" presents the most complex theories, both past and present, of physics; yet it remains clear and accessible. It will enlighten readers and expose them to the rich history of scientific thought and the complexities of the universe in which we live.','thetheoryofeverything.jpg', 7, 3),
(9780132350884, 'Clean Code','Robert C. Martin', 'Hardback', '2009', 'Science', false, 'Noted software expert Robert C. Martin presents a revolutionary paradigm with Clean Code: A Handbook of Agile Software Craftsmanship. Martin has teamed up with his colleagues from Object Mentor to distill their best agile practice of cleaning code "on the fly" into a book that will instill within you the values of a software craftsman and make you a better programmer-but only if you work at it.','cleancode.jpg', 2, 1),
(9780393609394, 'Astrophysics for People in a Hurry','Neil Degrasse Tyson', 'Paperback', '2018', 'Science', false, 'There\'s no better guide through mind-expanding questions such as what the nature of space and time is, how we fit within the universe, and how the universe fits within us than Neil deGrasse Tyson. Astrophysics for People in a Hurry reveals just what you need to be fluent and ready for the next cosmic headlines: from the Big Bang to black holes, from quarks to quantum mechanics and from the search for planets to the search for life in the universe.','astrophysicsforpeopleinahurry.jpg', 5, 1),
(9781846077852, 'Mary Berry\'s Baking Bible', 'Mary Berry', 'Hardback', '2010', 'Food & Drink', true, 'This definitive collection from the undisputed queen of cakes brings together all of Mary Berry\'s most mouth-watering baking recipes in a beautifully packaged edition. Filled with 250 foolproof recipes, from the classic Victoria Sponge, Very Best Chocolate Cake and Hazelnut Meringue Cake to tempting muffins, scones and bread and butter pudding, this is the most comprehensive baking cookbook you\'ll ever need.', 'maryberrysbakingbible.jpg', 3, 10),
(9780007415502, 'Kitchen Hero', 'Donal Skehan', 'Hardback', '2012', 'Food & Drink', false, 'Champion of the home cook, Donal Skehan, is back with a collection of tasty, easy-to-make dishes that cost less. Divided into 7 chapters, Donal covers everything from everyday suppers, soups, stews and pots, baking and desserts, roasts, healthy dishes and more.', 'kitchenhero.jpg', 2, 5),
(9780261102217, 'The Hobbit', ' J.R.R. Tolkein', 'Paperback', '1991', 'Fantasy', true, 'The Hobbit is a tale of high adventure, undertaken by a company of dwarves in search of dragon-guarded gold. A reluctant partner in this perilous quest is Bilbo Baggins, a comfort-loving unambitious hobbit, who surprises even himself by his resourcefulness and skill as a burglar.', 'thehobbit.jpg', 10, 8),
(9780091956141, 'The Martian', 'Andy Weir', 'Audiobook', '2014', 'Sci-Fi', false, 'A survival story for the 21st century and the international bestseller behind the major film from Ridley Scott, starring Matt Damon and Jessica Chastain.', 'themartian.jpg', 4, 8);

/*!40000 ALTER TABLE book ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-12-01  9:22:24


# USER TABLE

DROP TABLE IF EXISTS user;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE user (
  userID int(6) NOT NULL auto_increment,
  userFullName varchar(45) DEFAULT NULL,
  userPassword varchar(60) DEFAULT NULL,
  userImage varchar(45) DEFAULT NULL,
  memberSince date,
  userType varchar(20) NOT NULL,
  PRIMARY KEY (userID)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES user WRITE;
/*!40000 ALTER TABLE user DISABLE KEYS */;
INSERT INTO user VALUES 
(1, 'Bernie Stewart', 'pass1234', 'berniestewart.jpg', '2018-02-17', 'Librarian'),
(2, 'Kathy Donnelly', 'mypass12', 'kathydonnelly.jpg', '2019-01-16', 'Member'),
(3, 'Bob Ross', 'welcome1', 'bobross.jpg', '2018-11-27', 'Member'),
(4, 'John Smith', 'welcome12', 'johnsmith.jpg', curdate(), 'Member');

/*!40000 ALTER TABLE user ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

USE `library`;
DELIMITER $$
CREATE TRIGGER `default_date` BEFORE INSERT ON `user` FOR EACH ROW
if ( isnull(new.memberSince) ) then
 set new.memberSince=curdate();
end if;
$$
delimiter ;