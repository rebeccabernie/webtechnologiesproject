package com.ait.entities.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.core.Response;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.ait.library.dao.UserDAO;
import com.ait.library.dao.UtilsDAO;
import com.ait.library.entities.User;
import com.ait.library.rest.RestApplication;
import com.ait.library.rest.UserWS;


@RunWith(Arquillian.class)
public class UserIntegrationTest {
	
	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap.create(JavaArchive.class, "Test.jar")
				.addClasses(UserDAO.class, UserWS.class, User.class, RestApplication.class, UtilsDAO.class)
				//.addPackages(true, "com.ait")
				.addAsManifestResource("META-INF/persistence.xml", "persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
	} 

	@EJB
	private UserWS userWS;

	@EJB
	private UserDAO userDAO;

	@EJB
	private UtilsDAO utilsDAO;
	
	List<User> userList;
	User member1;
	User member2;
	User librarian1;
	
	final String REST_URL = "http://http://localhost:8080/WebTechLibrary/rest/users";
	
	@Before
	public void setUp() {
		utilsDAO.deleteUserTable();
		member1 = new User();
		member1.setUserID(5);
		member1.setUserFullName("Pat Byrne");
		member1.setUserImage("patbyrne.jpg");
		member1.setUserPassword("1234");
		member1.setMemberSince("2019-03-01");
		member1.setUserType("Member");
		userDAO.saveUser(member1);
		
		member2 = new User();
		member2.setUserID(6);
		member2.setUserFullName("Sally Bates");
		member2.setUserImage("sallybates.jpg");
		member2.setUserPassword("1234");
		member2.setUserType("Member");
		userDAO.saveUser(member2);
		
		librarian1 = new User();
		librarian1.setUserID(7);
		librarian1.setUserFullName("Bernie Stewart");
		librarian1.setUserImage("berniestewart.jpg");
		librarian1.setUserPassword("pass");
		librarian1.setUserType("Librarian");
		userDAO.saveUser(librarian1);
		
		userList = userDAO.getAllUsers();
	}
	
	@Test
	public void testGetAllMembers() {
		Response response = userWS.getMembers();
		userList = (List<User>) response.getEntity();
		assertEquals(2, userList.size());
		assertEquals(2, userWS.getTotalMemberCount().getEntity());
	}
	
	@Test
	public void testGetAllUsers() {
		Response response = userWS.getAllUsers();
		userList = (List<User>) response.getEntity();
		assertEquals(3, userList.size());
	}
	
	@Test
	public void testCreateUser() {
		User newUser = new User();
		newUser.setUserID(8);
		newUser.setUserFullName("Jim Norton");
		newUser.setUserImage("jimnorton.jpg");
		newUser.setUserPassword("newpass");
		newUser.setUserType("Member");
		userWS.saveUser(newUser);
		userList = (List<User>) userWS.getAllUsers().getEntity();
		assertEquals(4, userList.size());
	}
	
	@Test
	public void testUpdateUser() {
		Response response =  userWS.getUserByMembershipID(6);
		User user = (User)response.getEntity();
		assertEquals("Sally Bates", user.getUserFullName());
		
		user.setUserFullName("Sally A. Bates");
		userWS.updateUser(user);
		User updatedUser = ((User) userWS.getUserByMembershipID(6).getEntity());
		assertEquals("Sally A. Bates", updatedUser.getUserFullName());
	}
	
	@Test
	public void testRemoveUser() {
		Response response = userWS.removeUser(6);
		assertEquals(204, response.getStatus());
		userList = (List<User>) userWS.getAllUsers().getEntity();
		assertEquals(2, userList.size());
	}
	
	@After
	public void tearDown() {
		utilsDAO.deleteUserTable();
	}

}
