package com.ait.entities.test;

import static org.junit.Assert.*;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.core.Response;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.ait.library.dao.BookDAO;
import com.ait.library.dao.UtilsDAO;
import com.ait.library.entities.Book;
import com.ait.library.rest.BookWS;
import com.ait.library.rest.RestApplication;

@RunWith(Arquillian.class)
public class BookIntegrationTest {
	
	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap.create(JavaArchive.class, "Test.jar")
				.addClasses(BookDAO.class, BookWS.class, Book.class, RestApplication.class, UtilsDAO.class)
				//.addPackages(true, "com.ait")
				.addAsManifestResource("META-INF/persistence.xml", "persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
	} 

	@EJB
	private BookWS bookWS;

	@EJB
	private BookDAO bookDAO;

	@EJB
	private UtilsDAO utilsDAO;
	
	List<Book> bookList;
	Book book1;
	Book book2;
	Book book3;
	
	final String REST_URL = "http://http://localhost:8080/WebTechLibrary/rest/books";
	
	@Before
	public void setUp() {
		utilsDAO.deleteBookTable();
		book1 = new Book();
		book1.setIsbn("9781847670830");
		book1.setTitle("The Audacity of Hope");
		book1.setBestseller(true);
		book1.setCopiesAvailable("6");
		book1.setCopiesOnLoan("7");
		book1.setFormat("Paperback");
		bookDAO.saveBook(book1);
		
		book2 = new Book();
		book2.setIsbn("9780132350884");
		book2.setTitle("Clean Code");
		book2.setBestseller(false);
		book2.setCopiesAvailable("10");
		book2.setCopiesOnLoan("3");
		book2.setFormat("Hardback");
		bookDAO.saveBook(book2);
		
		bookList = bookDAO.getAllBooks();
	}

	@Test
	public void testGetAllBooks() {
		Response response = bookWS.getAllBooks();
		bookList = (List<Book>) response.getEntity();
		assertEquals(2, bookList.size());
	}
	
	@Test
	public void testCreateBook() {
		Book newBook = new Book();
		newBook.setIsbn("9781473664968");
		newBook.setTitle("Adventures of a Young Naturalist");
		newBook.setBestseller(false);
		newBook.setCopiesAvailable("4");
		newBook.setCopiesOnLoan("8");
		newBook.setFormat("Audiobook");
		bookDAO.saveBook(newBook);
		
		Response response = bookWS.getAllBooks();
		bookList = (List<Book>) response.getEntity();
		assertEquals(3, bookList.size());
	}
	
	@Test
	public void testGetBookByISBN() {
		Response response = bookWS.getBookByISBN("9780132350884");
		Book book = (Book) response.getEntity();
		assertEquals("Clean Code", book.getTitle());
	}
	
	@Test
	public void testGetBookQuickstats() {
		int onLoan = (int) bookWS.getQuickBookStats("countBooksCurrentlyOnLoan").getEntity();
		assertEquals(10, onLoan);
		int available = (int) bookWS.getQuickBookStats("countBooksCurrentlyInLibrary").getEntity();
		assertEquals(16, available);
		int total = (int) bookWS.getQuickBookStats("countTotalBooks").getEntity();
		assertEquals(26, total);
	}
	
	@Test
	public void testUpateBook() {
		Response response =  bookWS.getBookByISBN("9780132350884");
		Book book = (Book)response.getEntity();
		assertEquals("Clean Code", book.getTitle());
		
		book.setTitle("Clean Code 2nd Edition");
		bookWS.updateBook(book);
		Book updatedBook = ((Book) bookWS.getBookByISBN("9780132350884").getEntity());
		assertEquals("Clean Code 2nd Edition", updatedBook.getTitle());
		
	}
		
	@Test
	public void testRemoveBook() {
		Response response = bookWS.removeBook("9781847670830");
		assertEquals(204, response.getStatus());
		bookList = (List<Book>) bookWS.getAllBooks().getEntity();
		assertEquals(1, bookList.size());
		
	}
	
	@After
	public void tearDown() {
		utilsDAO.deleteBookTable();
	}

}
