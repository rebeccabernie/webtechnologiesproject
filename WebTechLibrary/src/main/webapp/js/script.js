var rootURL = "http://localhost:8080/WebTechLibrary/rest";

var selectedMemberID;
var selectedBookISBN;

$(document).ready(function() {
	$('[data-toggle="tooltip"]').tooltip();
	
	$('#logInModal').modal("show");
	
	$('#logInButton').click( function(){
		$('#userLoginError').css("visibility", "hidden");
		attemptLogin();
	});
	
	$('#logOutButton').click( function(){
		$('#logOutModal').modal("show");
	});
	
	$('#confirmLogOutButton').click( function(){
		logOutUser();
	});
	
	
	
	getQuickStats("countBooksCurrentlyOnLoan");
	getQuickStats("countBooksCurrentlyInLibrary");
	getQuickStats("countTotalBooks");
	getTotalMemberCount();
	
	setQuickStatUpdateTime();

	getAllBooks();
	getAllMembers();
	
	$( function() {
		$( "#addUpdateMemberSinceField" ).datepicker({
			dateFormat: "yy-mm-dd",
			maxDate: new Date()
		}).datepicker("setDate", new Date());
	});
	
	$('#newBookButton').click( function(){ clearBookFields(); });
	$('#saveBookButton').click( function(){ 
		var isbn = $("#updateAddBookISBNField").val();
		var title = $("#updateAddBookTitleField").val();
		var onLoan = $("#updateAddBookCopiesOnLoanField").val();
		var available = $("#updateAddBookCopiesAvailableField").val();
		
			if(fieldIsEmpty(isbn) || fieldIsEmpty(title) || fieldIsNotValidNumber(isbn) || fieldIsNotValidNumber(onLoan) || fieldIsNotValidNumber(available)){
				if(fieldIsEmpty(isbn)){
					$('#bookCreateUpdateError').text("Book ISBN cannot be left blank.");
					$('#bookCreateUpdateError').css("visibility", "visible");
				}
				if(fieldIsEmpty(title)){
					$('#bookCreateUpdateError').text("Title cannot be left blank.");
					$('#bookCreateUpdateError').css("visibility", "visible");
				}
				if(fieldIsNotValidNumber(isbn)){
					$('#bookCreateUpdateError').text("ISBN must be a valid number.");
					$('#bookCreateUpdateError').css("visibility", "visible");
				}
				if(fieldIsNotValidNumber(onLoan)){
					$('#bookCreateUpdateError').text("Copies on Loan must be a valid number.");
					$('#bookCreateUpdateError').css("visibility", "visible");
				}
				if(fieldIsNotValidNumber(available)){
					$('#bookCreateUpdateError').text("Copies Available must be a valid number.");
					$('#bookCreateUpdateError').css("visibility", "visible");
				}
			} else {
				if("Book Information" == $('#updateAddNewBookModalTitle').text()){
					updateBook();
					$('#bookCreateUpdateError').css("visibility", "hidden");
				} else {
					addBook();
					$('#bookCreateUpdateError').css("visibility", "hidden");
				}
			}
	});
	$('#deleteBookButton').click( function(){ deleteBook(); });
	
	$('#newUserButton').click( function(){ clearUserFields(); });
	$('#saveUserButton').click( function(){ 
		var userID = $("#addUpdateMemberIDField").val();
		var userName = $("#addUpdateMemberFullNameField").val();
		var modalTitle = $('#updateAddNewMemberModalTitle').text();
		var userPassword = $('#addMemberPasswordField').val();
		var confirmedPassword = $('#addMemberConfirmPasswordField').val();
		
		
		if(modalTitle == "Add New User"){
			if(fieldIsEmpty(userName) || passwordFieldsBlank(userPassword, confirmedPassword) || passwordFieldsDontMatch(userPassword, confirmedPassword)){
				if(fieldIsEmpty(userName)){
					$('#userCreateUpdateError').text("User Full Name cannot be left blank.");
					$('#userCreateUpdateError').css("visibility", "visible");
				}
				if(passwordFieldsBlank(userPassword, confirmedPassword)){
					$('#userCreateUpdateError').text("Password fields cannot be left blank.");
					$('#userCreateUpdateError').css("visibility", "visible");
				}
				if(passwordFieldsDontMatch(userPassword, confirmedPassword)){
					$('#userCreateUpdateError').text("Passwords must match.");
					$('#userCreateUpdateError').css("visibility", "visible");
				}
			} else {
				addNewUser();
			}
		} else if (modalTitle == "Membership Card"){
			if(fieldIsEmpty(userName)){
				$('#userCreateUpdateError').text("User Full Name cannot be left blank.");
				$('#userCreateUpdateError').css("visibility", "visible");
			} else {
				updateUser();
			}
		}
	});
	
	$('#deleteUserButton').click( function(){ deleteUser(); });
	
});

// LOGIN FUNCTIONALITY -------------------------------------------------------------------------------------------------------------------------------
var globalLoggedInUserType;
function attemptLogin(){
	var givenUserID = $('#userIDLoginField').val();
	var givenUserPassword = $('#userPasswordLoginField').val();
	
	if(givenUserID == "" || givenUserPassword == ""){
		$('#userLoginError').css("visibility", "visible");
	} else {
		$('#userLoginError').css("visibility", "hidden");
		$.ajax({
			type : 'GET',
			url : rootURL + "/users/id=" + givenUserID,
			dataType : "json",
			success : function(user) {
				if(user.userPassword == givenUserPassword){
					$('#userLoginError').css("visibility", "hidden");
					loginSuccessful(user);
				} else {
					$('#userLoginError').css("visibility", "visible");
				}
				
			},
			error: function(){
				$('#userLoginError').css("visibility", "visible");
			}
		});
	}		
}
function loginSuccessful(user){
	$('#logInModal').modal("hide");
	$('#userLoginError').css("visibility", "hidden");
	
	if(user.userType == "Librarian"){
		$('#cardsSection').css("display", "block");
		$('#cardsSection').css("visibility", "visible");
		$('#membersTableSection').css("display", "block");
		$('#membersTableSection').css("visibility", "visible");
		$('#booksTableSection').css("margin-top", "0px"); // reset to 0 in case member was logged in beforehand (will be off by 20px if not reset)
		$("#updateAddNewBookModal input, #updateAddNewBookModal select, #updateAddNewBookModal textarea").prop('disabled', false); // enable inputs in book information modal
		$('#newBookButton').css("visibility", "visible");
		$('#deleteBookButton').css("visibility", "visible");
		$('#saveBookButton').css("visibility", "visible");
		globalLoggedInUserType = "Librarian";
	} else {
		$('#booksTableSection').css("margin-top", "20px"); // looks too close to navbar if it's the only thing displayed (i.e. member logged in)
		$("#updateAddNewBookModal input, #updateAddNewBookModal select, #updateAddNewBookModal textarea").prop('disabled', true); // disable inputs in book information modal
		$('#newBookButton').css("visibility", "hidden");
		$('#deleteBookButton').css("visibility", "hidden");
		$('#saveBookButton').css("visibility", "hidden");
		globalLoggedInUserType = "Member";
	}
	
	$('#booksTableSection').css("display", "block");
	$('#booksTableSection').css("visibility", "visible");
	$('#pageFooter').css("display", "block");
	$('#pageFooter').css("visibility", "visible");
	
	$('#loggedInUserPicture').attr("src", "images/profilepictures/" + user.userImage);
	$('#loggedInUserFullName').text(user.userFullName);
	$('#loggedInUserType').text(user.userType);
}

// Log Out
function logOutUser(){
	$('#logOutModal').modal("hide");
	
	$('#cardsSection').css("display", "none");
	$('#cardsSection').css("visibility", "hidden");
	$('#membersTableSection').css("display", "none");
	$('#membersTableSection').css("visibility", "hidden");
	$('#booksTableSection').css("display", "none");
	$('#booksTableSection').css("visibility", "hidden");
	$('#pageFooter').css("display", "none");
	$('#pageFooter').css("visibility", "hidden");
	
	$('#loggedInUserPicture').attr("src", "images/profilepictures/default.jpg");
	$('#loggedInUserFullName').text("");
	$('#loggedInUserType').text("");
	
	$('#userIDLoginField').val("");
	$('#userPasswordLoginField').val("");
	$('#logInModal').modal("show");
}

// QUICK STATS (CARDS) ---------------------------------------------------------------------------------------------------------------------------------------

function setQuickStatUpdateTime(){
	var now = new Date();
	var minutes = now.getMinutes();
	if(minutes < 10)
		minutes = "0" + minutes;
	$('.quickStatsUpdated').text("Updated " + now.getDate()  + "/" + (now.getMonth()+1) + "/" + now.getFullYear() + ", " + now.getHours() + ":" + minutes);
	$('.quickStatsUpdated').css("font-size", "12px");
	$('.quickStatsUpdated').css("margin-bottom", "0");
}

function showSuccessfulToast(){
	var successToast = document.getElementById("successfulToast");
	successToast.className = "show";
	setTimeout(function(){ successToast.className = successToast.className.replace("show", ""); }, 4000);
}

var getQuickStats = function(stat) {
	$.ajax({
		type : 'GET',
		url : rootURL + "/books/quickstats/" + stat,
		dataType : "json",
		success : function(count) {
			$("#" + stat).text(count);
		}
	});
};

var getTotalMemberCount = function() {
	$.ajax({
		type : 'GET',
		url : rootURL + "/users/countTotalMembers",
		dataType : "json",
		success : function(count) {
			$('#countTotalMembers').text(count);
		}
	});
}

// POPULATE LISTS -------------------------------------------------------------------------------------------
var getAllBooks = function() {
	$.ajax({
		type : 'GET',
		url : rootURL + "/books",
		dataType : "json",
		success : function(data) {
			populateAllBooksDataTable(data);
		}
	});
}

function populateAllBooksDataTable(data) {
	$('#allBooksDataTable').dataTable().fnDestroy();
	$('#allBooksDataTable').empty();
	$('#allBooksDataTable').append("<thead> <tr class=\"text-muted\"> <th>ISBN</th> <th>Title</th> <th>Author</th> <th>Category</th> <th style=\"font-size: 12px\">Copies Available</th> </tr> </thead> <tbody> </tbody>");
	
	var table = $('#allBooksDataTable').dataTable( {
		"lengthMenu": [5, 10, 25, 50],
        "pageLength": 5,
		"bAutoWidth": false,
		"data" : data,
		"initComplete" : function() { // https://stackoverflow.com/a/37318054/7232648
			$('#allBooksDataTable').on("click", "tr[role='row']", function() {
				selectedBookISBN = $(this).children('td:first-child').text();	
				$('#updateAddNewBookModalTitle').text("Book Information");
				$.ajax({
					type : 'GET',
					url : rootURL + "/books/isbn=" + selectedBookISBN,
					dataType : "json",
					success : function(data) {
						if(globalLoggedInUserType == "Librarian"){
							$("#newBookButton").css("visibility","visible");
							$("#deleteBookButton").css("visibility","visible");
						}
						
						$("#updateAddBookISBNField").prop('disabled', true);
						
						$("#updateAddBookISBNField").val(data.isbn);
						$("#updateAddBookTitleField").val(data.title);
						$("#updateAddBookAuthorField").val(data.author);
						$("#updateAddBookFormatField").val(data.format);
						$("#updateAddBookYearField").val(data.year);
						$("#updateAddBookCategoryField").val(data.category);
						
						if(data.bestseller == 1)
							$("#bestsellerSelectedTrue").prop("checked", true);
						else
							$("#bestsellerSelectedFalse").prop("checked", true);
						
						$("#updateAddBookDisplayPicture").attr("src", "images/bookcovers/" + data.picture);
						$("#updateAddBookDescriptionField").val(data.description);
						$("#updateAddBookPictureField").val(data.picture);
						$("#updateAddBookCopiesOnLoanField").val(data.copiesOnLoan);
						$("#updateAddBookCopiesAvailableField").val(data.copiesAvailable);
						
						$('#bookCreateUpdateError').css("visibility", "hidden");
						$('#updateAddNewBookModal').modal("show");
					}
				});
				
				
			});
		},
		"columns" : [ {
			"data" : "isbn"
		}, {
			"data" : "title"
		}, {
			"data" : "author"
		}, {
			"data" : "category"
		}, {
			"data" : "copiesAvailable"
		} ]
	})
}

var getAllMembers = function() {
	$.ajax({
		type : 'GET',
		url : rootURL + "/users/members",
		dataType : "json",
		success : function(data) {
			populateAllMembersDataTable(data);
		}
	});
}

function populateAllMembersDataTable(data) {
	$('#allMembersDataTable').dataTable().fnDestroy();
	$('#allMembersDataTable').empty();
	$('#allMembersDataTable').append('<thead> <tr class="text-muted"> <th>Image</th> <th>ID</th> <th>Name</th> <th>Member Since</th> </tr> </thead> <tbody> </tbody>');
	
	var table = $('#allMembersDataTable').dataTable({
		"bAutoWidth": false,
		"data" : data,
		"initComplete" : function() { // https://stackoverflow.com/a/37318054/7232648
			$("#allMembersDataTable").on("click", "tr[role='row']", function() {
				selectedMemberID = $(this).children('td:nth-child(2)').text();
				hideNewUserFields();
				$.ajax({
					type : 'GET',
					url : rootURL + "/users/id=" + selectedMemberID,
					dataType : "json",
					success : function(data) {
						$("#addUpdateMemberDisplayPicture").attr("src", "images/profilepictures/" + data.userImage);
						$("#addUpdateMemberIDField").val(data.userID);
						$("#addUpdateMemberFullNameField").val(data.userFullName);
						$("#addUpdateMemberSinceField").val(data.memberSince);
						$("#addUserTypeField").val(data.userType);
						$("#addMemberImage").val(data.userImage);
						
						$('#userCreateUpdateError').css("visibility", "hidden");
						$('#updateAddNewMemberModal').modal("show");
					}
				});
			});
		},
		"columns" : [ {
			"data" : "userImage", render: function(data) {
		        return '<img src="images/profilepictures/'+data+'" style="width: 50px">'
		      }
		},{
			"data" : "userID"
		}, {
			"data" : "userFullName"
		}, {
			"data" : "memberSince"
		} ]
	})
}

// ADD / UPDATE / DELETE BOOK
function clearBookFields(){
	$("#updateAddNewBookModalTitle").text("Add New Book");
	
	$("#updateAddBookISBNField").prop('disabled', false);
	$("#updateAddBookISBNField").val("");
	$("#updateAddBookTitleField").val("");
	$("#updateAddBookAuthorField").val("");
	$("#updateAddBookFormatField").val("Paperback");
	$("#updateAddBookYearField").val("");
	$("#updateAddBookCategoryField").val("");
	$("#bestsellerSelectedFalse").prop("checked", true);
	$("#updateAddBookDisplayPicture").attr("src", "images/bookcovers/default.jpg");
	$("#updateAddBookDescriptionField").val("");
	$("#updateAddBookPictureField").val("");
	$("#updateAddBookCopiesOnLoanField").val("");
	$("#updateAddBookCopiesAvailableField").val("");
	
	$("#newBookButton").css("visibility","hidden");
	$('#deleteBookButton').css("visibility", "hidden");
}

var bookFormToJSON = function() {
	var isbn = $('#updateAddBookISBNField').val()
	var title = $('#updateAddBookTitleField').val();
	var author = $('#updateAddBookAuthorField').val();
	var format = $('#updateAddBookFormatField').val();
	var year = $('#updateAddBookYearField').val();
	var category = $('#updateAddBookCategoryField').val();
	var bestseller = $('input[name=bestsellerRadios]:checked').val();
	var description = $('#updateAddBookDescriptionField').val();
	var picture = $('#updateAddBookPictureField').val();
	var copiesAvailable = $('#updateAddBookCopiesAvailableField').val();
	var copiesOnLoan = $('#updateAddBookCopiesOnLoanField').val();
		
	if(picture == null || picture == ""){
		picture = "default.jpg";
	}
	
	var JSONstring = '{"isbn" : "' + isbn + '"';
	if (title != null && title != "")
		JSONstring += ', "title" : "' + title + '"';
	if(author != null && author != "")
		JSONstring += ', "author" : "' + author + '"';
	if(format != null && format != "")
		JSONstring += ', "format" : "' + format + '"';
	if(year != null && year != "")
		JSONstring += ', "year" : "' + year + '"';
	if(category != null && category != "")
		JSONstring += ', "category" : "' + category + '"';
	if(bestseller != null && bestseller != "")
		JSONstring += ', "bestseller" : "' + bestseller + '"';
	if(description != null && description != "")
		JSONstring += ', "description" : "' + description.replace(/"/g, "\\\"") + '"';
	if(picture != null && picture != "")
		JSONstring += ', "picture" : "' + picture + '"';
	if(copiesAvailable != null && copiesAvailable != "")
		JSONstring += ', "copiesAvailable" : "' + copiesAvailable + '"';
	if(copiesOnLoan != null && copiesOnLoan != "")
		JSONstring += ', "copiesOnLoan" : "' + copiesOnLoan + '"';
	
	JSONstring += '};';
	return JSONstring;
}

function addBook(){
	$.ajax({
		type : 'POST',
		contentType : "application/json",
		url : rootURL + '/books',
		dataType : "json",
		data : bookFormToJSON(),
		success : function(data, textStatus, jqXHR) {
			getAllBooks();
			getQuickStats("countBooksCurrentlyOnLoan");
			getQuickStats("countBooksCurrentlyInLibrary");
			getQuickStats("countTotalBooks");
			$('#updateAddNewBookModal').modal("hide");
			$('#successfulToast').text("Book added successfully!");
			showSuccessfulToast();
		}, error : function(){
			$('#bookCreateUpdateError').text("ISBN already exists.");
			$('#bookCreateUpdateError').css("visibility", "visible");
		}
	});
}

function updateBook(){
	$.ajax({
		type : 'PUT',
		contentType : "application/json",
		url : rootURL + '/books/isbn=' + selectedBookISBN,
		dataType : "json",
		data : bookFormToJSON(),
		success : function(data, textStatus, jqXHR) {
			getAllBooks();
			getQuickStats("countBooksCurrentlyOnLoan");
			getQuickStats("countBooksCurrentlyInLibrary");
			getQuickStats("countTotalBooks");
			$('#updateAddNewBookModal').modal("hide");
			$('#successfulToast').text("Book updated successfully!");
			showSuccessfulToast();
		}
	});
}

function deleteBook(){
	$.ajax({
		type : 'DELETE',
		url : rootURL + '/books/isbn=' + selectedBookISBN,
		success : function(data, textStatus, jqXHR) {
			getAllBooks();
			getQuickStats("countBooksCurrentlyOnLoan");
			getQuickStats("countBooksCurrentlyInLibrary");
			getQuickStats("countTotalBooks");
			$('#updateAddNewBookModal').modal("hide");
			$('#successfulToast').text("Book deleted successfully!");
			showSuccessfulToast();
		}, error : function(data){
			alert("Error deleting book");
		}
	})
}

// ADD / UPDATE / DELETE USER
function hideNewUserFields(){
	$('#addUpdateMemberIDField').css("display", "block");
	$('#addUpdateMemberIDField').css("visibility", "visible");
	$('#memberIDLabel').css("display", "block");
	$('#memberIDLabel').css("visibility", "visible");
	
	
	$("#addUpdateMemberIDField").prop('disabled', true);
	$('#updateAddNewMemberModalTitle').text("Membership Card");
	$("#chooseUserPassword").css("display", "none");
	$("#confirmUserPassword").css("display", "none");
	$("#chooseUserType").css("display", "none");
	$("#chooseUserPassword").css("visibility", "hidden");
	$("#confirmUserPassword").css("visibility", "hidden");
	$("#chooseUserType").css("visibility", "hidden");
	
	$('#newUserButton').css("visibility", "visible");
	$('#deleteUserButton').css("visibility", "visible");
}
function showHiddenUserFields(){
	$('#addUpdateMemberIDField').css("display", "none");
	$('#addUpdateMemberIDField').css("visibility", "hidden");
	$('#memberIDLabel').css("display", "none");
	$('#memberIDLabel').css("visibility", "hidden");
	
	$("#chooseUserPassword").css("display", "block");
	$("#confirmUserPassword").css("display", "block");
	$("#chooseUserType").css("display", "block");
	$("#chooseUserPassword").css("visibility", "visible");
	$("#confirmUserPassword").css("visibility", "visible");
	$("#chooseUserType").css("visibility", "visible");
	
	$('#newUserButton').css("visibility", "hidden");
	$('#deleteUserButton').css("visibility", "hidden");
}

function clearUserFields(){
	$('#userCreateUpdateError').css("visibility", "hidden");
	
	$("#updateAddNewMemberModalTitle").text("Add New User");
	
	var now = new Date();
	var today = now.getFullYear() + "-" + (now.getMonth()+1) + "-" + now.getDate();
	
	
	$("#addUpdateMemberIDField").prop('disabled', false);
	
	$("#addUpdateMemberIDField").val("");
	$("#addUpdateMemberDisplayPicture").attr("src", "images/profilepictures/default.jpg");
	$("#addUpdateMemberIDField").val("");
	$("#addUpdateMemberFullNameField").val("");
	$("#addUpdateMemberSinceField").val(today);
	$("#addUserTypeField").val("Member");
	$("#addMemberImage").val("");
	showHiddenUserFields();
}

var userFormToJSON = function(){
	var userID = $('#addUpdateMemberIDField').val()
	var userFullName = $('#addUpdateMemberFullNameField').val();
	var userPassword = $('#addMemberPasswordField').val();
	var userImage = $('#addMemberImage').val();
	var memberSince = $('#addUpdateMemberSinceField').val();
	var userType = $('#addUserTypeField').val();
	
	if(userImage == null || userImage == ""){
		userImage = "default.jpg";
	}
	
	var JSONstring = '{"userID" : "' + userID + '"';
	if (userFullName != null && userFullName != "")
		JSONstring += ', "userFullName" : "' + userFullName + '"';
	if(userPassword != null && userPassword != "")
		JSONstring += ', "userPassword" : "' + userPassword + '"';
	if(userImage != null && userImage != "")
		JSONstring += ', "userImage" : "' + userImage + '"';
	if(memberSince != null && memberSince != "")
		JSONstring += ', "memberSince" : "' + memberSince + '"';
	if(userType != null && userType != "")
		JSONstring += ', "userType" : "' + userType + '"';
	
	JSONstring += '};'
	return JSONstring;
}

function addNewUser(){
	$.ajax({
		type : 'POST',
		contentType : "application/json",
		url : rootURL + '/users',
		dataType : "json",
		data : userFormToJSON(),
		success : function(data, textStatus, jqXHR) {
			getAllMembers();
			getTotalMemberCount();
			$('#updateAddNewMemberModal').modal("hide");
			$('#successfulToast').text("User added successfully!");
			showSuccessfulToast();
		}
	});
}

function updateUser(){
	$.ajax({
		type : 'PUT',
		contentType : "application/json",
		url : rootURL + '/users/id=' + selectedMemberID,
		dataType : "json",
		data : userFormToJSON(),
		success : function(data, textStatus, jqXHR) {
			getAllMembers();
			getTotalMemberCount();
			$('#updateAddNewMemberModal').modal("hide");
			$('#successfulToast').text("User updated successfully!");
			showSuccessfulToast();
		}
	});
}

function deleteUser(){
	$.ajax({
		type : 'DELETE',
		url : rootURL + '/users/id=' + selectedMemberID,
		success : function(data, textStatus, jqXHR) {
			getAllMembers();
			getTotalMemberCount();
			$('#updateAddNewMemberModal').modal("hide");
			$('#successfulToast').text("User deleted successfully!");
			showSuccessfulToast();
		}, error : function(data){
			alert("Error deleting user");
		}
	})
}

// FORM HANDLING ----------------------------------------------------------------------------------------
var fieldIsEmpty = function(field){
	return (field == null || field == "");
}

var fieldIsNotValidNumber = function(field){
	return (isNaN(field) || field == null || field == "" || field > 9999999999999 || field < 0);
}

var numberFieldTooLongOrShort = function(field){
	return (field > 999999 || field < 0);
}

var passwordFieldsBlank = function (userPassword, confirmedPassword){
	return (userPassword == "" || confirmedPassword == "");
}

var passwordFieldsDontMatch = function(userPassword, confirmedPassword){
	return (userPassword != confirmedPassword);
}

var isbnAlreadyExists = function(isbn){
	$.ajax({
		type : 'GET',
		url : rootURL + "/books/isbn=" + isbn,
		dataType : "json",
		success : function(data) {
			if (data == null) {
				return false;
			} else {
				$('#bookCreateUpdateError').text("This ISBN already exists. Please try again.");
				$('#bookCreateUpdateError').css("visibility", "visible");
				return true;
			}				
		},
		error : function(jqXHR, textStatus, errorThrown) {
			$('#bookCreateUpdateError').text("This ISBN already exists. Please try again.");
			$('#bookCreateUpdateError').css("visibility", "visible");
			return true;
		}
	});
}