package com.ait.library.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ait.library.dao.BookDAO;
import com.ait.library.entities.Book;

@Path("/books")
@Stateless
@LocalBean
public class BookWS {
	
	@EJB
	private BookDAO bookDAO;
	
	// CREATE -----------------------------------------------------------------------
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	public Response saveBook(Book book) {
		bookDAO.saveBook(book);
		return Response.status(201).entity(book).build();
	}
	
	// READ -------------------------------------------------------------------------
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getAllBooks() {
		List<Book> books = bookDAO.getAllBooks();
		return Response.status(200).entity(books).build();
	}
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/isbn={isbn}")
	public Response getBookByISBN(@PathParam("isbn") String isbn) {
		Book book = bookDAO.getBookByISBN(isbn);
		if (book == null) {
			return Response.status(204).build();
		}
		return Response.status(200).entity(book).build();
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/quickstats/{stat}")
	public Response getQuickBookStats(@PathParam("stat") String stat) {
		int count = bookDAO.getQuickBookStats(stat);
		return Response.status(200).entity(count).build();
	}
	
	// UPDATE -----------------------------------------------------------------------
	@PUT
	@Path("/isbn={isbn}")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	public Response updateBook(Book book) {
		bookDAO.updateBook(book);
		return Response.status(200).entity(book).build();
	}
	
	// DELETE -----------------------------------------------------------------------
	@DELETE @Path("/isbn={isbn}")
	@Produces({MediaType.APPLICATION_JSON})
	public Response removeBook(@PathParam("isbn") String isbn) {
		bookDAO.removeBook(isbn);
		return Response.status(204).build();
	}

}
