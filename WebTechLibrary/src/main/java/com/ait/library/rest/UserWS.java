package com.ait.library.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ait.library.dao.UserDAO;
import com.ait.library.entities.User;

@Path("/users")
@Stateless
@LocalBean
public class UserWS {

	@EJB
	private UserDAO userDAO;

	// CREATE
	// -----------------------------------------------------------------------
	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response saveUser(User user) {
		userDAO.saveUser(user);
		return Response.status(201).entity(user).build();
	}

	// READ
	// -------------------------------------------------------------------------
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getAllUsers() {
		List<User> users = userDAO.getAllUsers();
		return Response.status(200).entity(users).build();
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/id={id}")
	public Response getUserByMembershipID(@PathParam("id") int id) {
		User user = userDAO.getUserByMembershipID(id);
		return Response.status(200).entity(user).build();
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/countTotalMembers")
	public Response getTotalMemberCount() {
		int count = userDAO.getTotalMemberCount();
		return Response.status(200).entity(count).build();
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/members")
	public Response getMembers() {
		List<User> members = userDAO.getMembers();
		return Response.status(200).entity(members).build();
	}

	// UPDATE ------------------------------------------------------------------
	@PUT
	@Path("/id={id}")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response updateUser(User user) {
		userDAO.updateUser(user);
		return Response.status(200).entity(user).build();
	}

	// DELETE
	// -----------------------------------------------------------------------
	@DELETE
	@Path("/id={id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response removeUser(@PathParam("id") int id) {
		userDAO.removeUser(id);
		return Response.status(204).build();
	}
}
