package com.ait.library.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Book {
	
	@Id
	private String isbn;
	private String title;
	private String author;
	private String format;
	private String year;
	private String category;
	private boolean bestseller;
	private String description;
	private String picture;
	private String copiesAvailable;
	private String copiesOnLoan;
	
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public boolean isBestseller() {
		return bestseller;
	}
	public void setBestseller(boolean bestseller) {
		this.bestseller = bestseller;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	public String getCopiesAvailable() {
		return copiesAvailable;
	}
	public void setCopiesAvailable(String copiesAvailable) {
		this.copiesAvailable = copiesAvailable;
	}
	public String getCopiesOnLoan() {
		return copiesOnLoan;
	}
	public void setCopiesOnLoan(String copiesOnLoan) {
		this.copiesOnLoan = copiesOnLoan;
	}
	
}
