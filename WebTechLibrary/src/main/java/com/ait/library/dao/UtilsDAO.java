package com.ait.library.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Stateless
@LocalBean
public class UtilsDAO {
	@PersistenceContext
    private EntityManager entityManager;
    
	public void deleteUserTable(){
		entityManager.createQuery("DELETE FROM User").executeUpdate();	
	}
	
	public void deleteBookTable(){
		entityManager.createQuery("DELETE FROM Book").executeUpdate();
	}

}
