package com.ait.library.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.ait.library.entities.Book;

@Stateless
@LocalBean
public class BookDAO {
	
	@PersistenceContext
	private EntityManager em;

	// GET ----------------------------------------------------------------
	public List<Book> getAllBooks() {
		javax.persistence.Query query = em.createQuery("Select b from Book b");
		return query.getResultList();
	}
	
	public Book getBookByISBN(String isbn) {
		Book b = em.find(Book.class, isbn);
		return b;
	}
	
	public List<Book> getBooksMatchingSearch(String search) {
		javax.persistence.Query query = em.createQuery("Select b from Book b where b.title LIKE ?1");
		query.setParameter(1, "%" + search + "%");
		return query.getResultList();
	}
	
	public int getQuickBookStats(String stat) {
		int count= 0;
		switch (stat) {
		case "countBooksCurrentlyOnLoan":
			for (Book b : getAllBooks()) {
				count += Integer.valueOf(b.getCopiesOnLoan());
			}
			break;
		case "countBooksCurrentlyInLibrary":
			for (Book b : getAllBooks()) {
				count += Integer.valueOf(b.getCopiesAvailable());
			}
			break;
		case "countTotalBooks":
			for (Book b : getAllBooks()) {
				count += Integer.valueOf(b.getCopiesOnLoan()) + Integer.valueOf(b.getCopiesAvailable());
			}
			break;
		default:
			System.err.println("Invalid choice.");
			break;
		}
		return count;
	}
	
	// OTHER ---------------------------------------
	public void saveBook(Book book) {
		em.persist(book);
	}

	public void updateBook(Book book) {
		em.merge(book);
	}

	public void removeBook(String isbn) {
		em.remove(getBookByISBN(isbn));
	}

}
