package com.ait.library.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.ait.library.entities.User;

@Stateless
@LocalBean
public class UserDAO {
	
	@PersistenceContext
	private EntityManager em;
	
	public List<User> getAllUsers() {
		javax.persistence.Query query = em.createQuery("Select u from User u");
		return query.getResultList();
	}
	public int getTotalMemberCount() {
		javax.persistence.Query query = em.createQuery("Select u from User u where u.userType LIKE ?1");
		query.setParameter(1, "Member");
		return query.getResultList().size();
	}
	public List<User> getMembers() {
		javax.persistence.Query query = em.createQuery("Select u from User u where u.userType LIKE ?1");
		query.setParameter(1, "Member");
		return query.getResultList();
	}
	
	public void saveUser(User user) {
		em.persist(user);
	}

	public User getUserByMembershipID(int id) {
		return em.find(User.class, id);
	}

	public void updateUser(User user) {
		em.merge(user);
	}

	public void removeUser(int id) {
		em.remove(getUserByMembershipID(id));
	}
}
